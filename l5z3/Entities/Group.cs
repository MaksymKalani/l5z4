﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace l5z3.Entities
{
    public class Group
    {
        [Key]
        public int Id { get; set; }
        [MinLength(3)]
        [MaxLength(20)]
        public string Name { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
