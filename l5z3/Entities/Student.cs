﻿using System.ComponentModel.DataAnnotations;

namespace l5z3.Entities
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [MinLength(3)]
        [MaxLength(20)]
        public string Name { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
