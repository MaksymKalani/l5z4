﻿using l5z3.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace l5z3
{
    class Program
    {
        static void PrintStudent(Student student)
        {
            Console.WriteLine($"{student.Id} {student.Name} {student.Group.Name}");
        }
        static void Main(string[] args)
        {
            using(var context = new StudentsContext())
            {
                Console.WriteLine("First()");
                var first = context.Students.Include(x=>x.Group).First();
                PrintStudent(first);

                Console.WriteLine("OrderBy()");
                var ordered = context.Students.Include(x => x.Group).OrderBy(x => x.Name).ToList();
                foreach(var s in ordered)
                {
                    PrintStudent(s);
                }

                Console.WriteLine("Count()");
                Console.WriteLine($"{context.Students.Count()}");

                Console.WriteLine("Min()");
                Console.WriteLine($"{context.Students.Min(x=> x.Id)}");

                Console.WriteLine("Max()");
                Console.WriteLine($"{context.Students.Max(x => x.Id)}");

                Console.WriteLine("Average()");
                Console.WriteLine($"{context.Students.Average(x => x.Id)}");
            }
            
        }
    }
}
