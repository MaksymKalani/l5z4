﻿using l5z3.Entities;
using Microsoft.EntityFrameworkCore;

namespace l5z3
{
    public class StudentsContext : DbContext
    {
        public StudentsContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=efbasicsappdb;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>().HasData(
                new Group { Id = 1, Name = "IH-14"},
                new Group { Id = 2, Name = "NH-88" }
                );

            modelBuilder.Entity<Student>().HasData(
                new Student { GroupId = 1, Id = 1, Name = "Oleg"},
                new Student { GroupId = 1, Id = 2, Name = "Anton" },
                new Student { GroupId = 2, Id = 3, Name = "Vasya" },
                new Student { GroupId = 2, Id = 4, Name = "Petya" },
                new Student { GroupId = 2, Id = 5, Name = "Abdullah" },
                new Student { GroupId = 2, Id = 6, Name = "Ilya" }
                );
        }
    }
}

